﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BKT.Models
{
    public class Account
    {
    
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Accountname { get; set; }
        public Customer? Customer { get; set; }
    }
}
