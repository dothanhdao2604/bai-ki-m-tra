﻿using System.ComponentModel.DataAnnotations;

namespace BKT.Models
{
    public class Customer
    {
        
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ContactandAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
