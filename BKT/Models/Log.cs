﻿using System.ComponentModel.DataAnnotations;

namespace BKT.Models
{
    public class Log
    {
        
        public int Id { get; set; }
        public int TransactionalId { get; set; }
        public DateOnly Logindate { get; set; }
        public TimeOnly Logintime { get; set; }
        public Transaction? Transactions { get; set; }
    }
}
