﻿using Microsoft.Identity.Client;
using System.ComponentModel.DataAnnotations;

namespace BKT.Models
{
    public class Report
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public Account? Account { get; set; }
        public int LogsId { get; set; }
        public Log? Log { get; set; }
        public int TransactionalId { get; set; }
        public Transaction? Transaction { get; set; }
        public string Reportname { get; set; }
        public DateOnly Reportdate { get; set; }
        
        
        
    }
}